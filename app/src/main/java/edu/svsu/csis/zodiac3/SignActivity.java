package edu.svsu.csis.zodiac3;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class SignActivity extends AppCompatActivity {

    public static final String EXTRA_SIGNNO = "signNo";
    private DailyHoroscope zodiacApi = new DailyHoroscope();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        int signNo = (int) getIntent().getExtras().get(EXTRA_SIGNNO);


        try {

            SQLiteOpenHelper signDatabaseHelper = new SignDatabaseHelper(this);
            SQLiteDatabase db = signDatabaseHelper.getReadableDatabase();
            Cursor cursor = db.query("SIGN", new String[] {"NAME", "DESCRIPTION", "SYMBOL", "MONTH", "IMAGE_RESOURCE_ID"}, "_id = ?", new String[] {Integer.toString(signNo)}, null, null, null, null);

            if(cursor.moveToFirst()) {


                String nameText = cursor.getString(0);
                String descriptionText = cursor.getString(1);
                String symbolText = cursor.getString(2);
                String monthText = cursor.getString(3);
                int photoID = cursor.getInt(4);
                String dailyHoroscope = zodiacApi.getDailyHoroscope(nameText);


                TextView name = (TextView) findViewById(R.id.name);
                name.setText(nameText);


                TextView description = (TextView) findViewById(R.id.description);
                description.setText(descriptionText);


                TextView symbol = (TextView) findViewById(R.id.symbol);
                symbol.setText(symbolText);


                TextView month = (TextView) findViewById(R.id.month);
                month.setText(monthText);

                ImageView photo = (ImageView) findViewById(R.id.photo);
                photo.setImageResource(photoID);
                photo.setContentDescription(nameText);

                TextView daily = (TextView) findViewById(R.id.daily);
                daily.setText(dailyHoroscope);

            }

            cursor.close();
            db.close();



        }catch(SQLiteException ex){
            Toast toast = Toast.makeText(this, "DATABASE UNAVAILABLE", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);


        int signNo = (int)getIntent().getExtras().get(EXTRA_SIGNNO);
        edu.svsu.csis.zodiac2.Sign sign = edu.svsu.csis.zodiac2.Sign.signs[signNo];

        ImageView photo = (ImageView)findViewById(R.id.photo);
        photo.setImageResource(sign.getImageResourceId());
        photo.setContentDescription(sign.getName());

        TextView name = (TextView)findViewById(R.id.name);
        name.setText(sign.getName());

        TextView description = (TextView)findViewById(R.id.description);
        description.setText(sign.getDescription());

        TextView symbol = (TextView)findViewById(R.id.symbol);
        symbol.setText(sign.getSymbol());

        TextView month = (TextView)findViewById(R.id.month);
        month.setText(sign.getMonth());

    }*/


}


