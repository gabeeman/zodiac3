package edu.svsu.csis.zodiac3;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SignDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "sign";
    private static final int DB_VERSION = 2;

    SignDatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("CREATE TABLE SIGN (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " NAME TEXT, " +
                "DESCRIPTION TEXT, " +
                "SYMBOL TEXT, " +
                "MONTH TEXT," +
                "IMAGE_RESOURCE_ID INTEGER);");
        insertSign(db, "Aries", "Courageous and Energetic", "Ram", "April", R.drawable.aries);
        insertSign(db, "Taurus", "Known for being reliable, practical, ambitious, and sensual", "Bull", "May", R.drawable.taurus);
        insertSign(db, "Gemini", "Gemini-born are clever and intellectual", "Twins", "June", R.drawable.gemini);
        insertSign(db, "Cancer", "Tenacious, loyal and sympathetic.", "Crab", "July", R.drawable.cancer);
        insertSign(db, "Leo", "Warm, action-oriented and driven by the desire to be loved and admired.", "Lion", "August", R.drawable.leo);
        insertSign(db, "Virgo", "Methodical, meticulous, analytical and mentally astute.", "Virgin", "September", R.drawable.virgo);
        insertSign(db, "Libra", "Librans are famous for maintaining balance and harmony.", "Scales", "October", R.drawable.libra);
        insertSign(db, "Scorpio", "String willed and mysterious.", "Scorpion", "November", R.drawable.scorpio);
        insertSign(db, "Sagittarius", "Born adventurers", "Archer", "December", R.drawable.sagittarius);
        insertSign(db, "Capicorn", "The most determined sign in the Zodiac", "Goat", "January", R.drawable.capricorn);
        insertSign(db, "Aquarius", "Humanitarians to the core", "Water Bearer", "February", R.drawable.aquarius);
        insertSign(db, "Pisces", "Proverbial dreamers of the Zodiac", "Fish", "March", R.drawable.pisces);
    };

    public void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion){
        if(oldVersion < 1){
            db.execSQL("CREATE TABLE SIGN (" +
                    "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " NAME TEXT, " +
                    "DESCRIPTION TEXT, " +
                    "SYMBOL TEXT, " +
                    "MONTH TEXT," +
                    "IMAGE_RESOURCE_ID INTEGER);");
            insertSign(db, "Aries", "Courageous and Energetic", "Ram", "April", R.drawable.aries);
            insertSign(db, "Taurus", "Known for being reliable, practical, ambitious, and sensual", "Bull", "May", R.drawable.taurus);
            insertSign(db, "Gemini", "Gemini-born are clever and intellectual", "Twins", "June", R.drawable.gemini);
            insertSign(db, "Cancer", "Tenacious, loyal and sympathetic.", "Crab", "July", R.drawable.cancer);
            insertSign(db, "Leo", "Warm, action-oriented and driven by the desire to be loved and admired.", "Lion", "August", R.drawable.leo);
            insertSign(db, "Virgo", "Methodical, meticulous, analytical and mentally astute.", "Virgin", "September", R.drawable.virgo);
            insertSign(db, "Libra", "Librans are famous for maintaining balance and harmony.", "Scales", "October", R.drawable.libra);
            insertSign(db, "Scorpio", "String willed and mysterious.", "Scorpion", "November", R.drawable.scorpio);
            insertSign(db, "Sagittarius", "Born adventurers", "Archer", "December", R.drawable.sagittarius);
            insertSign(db, "Capicorn", "The most determined sign in the Zodiac", "Goat", "January", R.drawable.capricorn);
            insertSign(db, "Aquarius", "Humanitarians to the core", "Water Bearer", "February", R.drawable.aquarius);
            insertSign(db, "Pisces", "Proverbial dreamers of the Zodiac", "Fish", "March", R.drawable.pisces);
        }
        if(oldVersion<2){
            db.execSQL("ALTER TABLE SIGN ADD COLUMN FAVORITE NUMERIC;");
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        updateMyDatabase(sqLiteDatabase, oldVersion, newVersion);
    }



    private static void insertSign(SQLiteDatabase db, String name, String description, String symbol, String month, int imageResourceID){

        ContentValues signValues = new ContentValues();
        signValues.put("NAME", name);
        signValues.put("DESCRIPTION", description);
        signValues.put("SYMBOL", symbol);
        signValues.put("MONTH", month);
        signValues.put("IMAGE_RESOURCE_ID", imageResourceID);
        db.insert("SIGN", null, signValues);

    }
}
